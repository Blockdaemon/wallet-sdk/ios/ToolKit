// Copyright © Blockdaemon All rights reserved.

import Foundation
import Extensions

/// Protocol description for a log statement destination (e.g. console, file, remote, etc.)
public protocol LogDestination {

    /// Logs a statement to this destination.
    ///
    /// - Parameters:
    ///   - statement: the statement to log
    func log(statement: String, level: LogLevel)
}

/// A destination wherein log statements are outputted to standard output (i.e. XCode's console)
public class ConsoleLogDestination: LogDestination {
    public func log(statement: String, level: LogLevel) {
        print(statement)
    }
}

/// Enumerates the level/severity of a log statement
public enum LogLevel {
    case debug, info, warning, error
}

extension LogLevel {

    public var emoji: String {
        switch self {
        case .debug:
            return "🏗"
        case .info:
            return "ℹ️"
        case .warning:
            return "⚠️"
        case .error:
            return "🛑"
        }
    }
}

/// Class in charge of logging debug/info/warning/error messages to a `LogDestination`.
@objc public class Logger: NSObject {

    public enum Verbosity {
        case very
        case some
        case none
    }

    internal var destinations = [LogDestination]()

    private lazy var timestampFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        return formatter
    }()

    public static let shared: Logger = {
        let logger = Logger()
        #if DEBUG
        logger.destinations.append(ConsoleLogDestination())
        #endif
        return logger
    }()

    @objc public class func sharedInstance() -> Logger { shared }

    public var verbosity: Verbosity = .very

    // MARK: - Public

    public func debug(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        log(message, level: .debug, file: file, function: function, line: line)
    }

    public func info(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        log(message, level: .info, file: file, function: function, line: line)
    }

    public func warning(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        log(message, level: .warning, file: file, function: function, line: line)
    }

    public func error(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        log(message, level: .error, file: file, function: function, line: line)
    }

    public func error(
        _ error: Error,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        log(String(describing: error), level: .error, file: file, function: function, line: line)
    }

    public func log(
        _ message: String,
        level: LogLevel,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        destinations.forEach {
            let statement = formatMessage(
                message,
                level: level,
                file: file,
                function: function,
                line: line
            )
            $0.log(statement: statement, level: level)
        }
    }

    // MARK: - Private

    private func formatMessage(
        _ message: String,
        level: LogLevel,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) -> String {
        switch verbosity {
        case .very:
            let timestamp = timestampFormatter.string(from: Date())
            let logLevelTitle = "\(level)".uppercased()
            return "\(timestamp) \(level.emoji) \(logLevelTitle): \n \(message) \(CodeLocation(function, file, line))"
        case .some:
            return "\(level.emoji) \(message) \(CodeLocation(function, file, line))"
        case .none:
            return message
        }
    }
}

