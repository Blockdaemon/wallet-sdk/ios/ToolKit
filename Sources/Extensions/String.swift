// Copyright © Blockdaemon All rights reserved.

import Foundation

extension String {
    public var base64URLUnescaped: String {
        let replaced = replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        /// https://stackoverflow.com/questions/43499651/decode-base64url-to-base64-swift
        let padding = replaced.count % 4
        if padding > 0 {
            return replaced + String(repeating: "=", count: 4 - padding)
        }
        return replaced
    }
}
