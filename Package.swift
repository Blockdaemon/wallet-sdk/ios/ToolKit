// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ToolKit",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "ToolKit",
            targets: ["ToolKit"]),
    ],
    dependencies: [
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Extensions",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Errors",
            exact: "1.0.0"
        ),
    ],
    targets: [
        .target(
            name: "ToolKit",
            dependencies: [
                "Extensions",
                "Errors"
            ]),
        .testTarget(
            name: "ToolKitTests",
            dependencies: ["ToolKit"]),
    ]
)
